package com.worldclock.automation.base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public abstract class WorldclockBaseTest {
	public WebDriver driver = new FirefoxDriver();
	public Actions build = new Actions(driver);
	public String url = "http://www.worldclock.com";

	// common things that need to open before running any program.
	@BeforeSuite
	public void init() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(url);

	}

	// Teardown to close and kill all the open browser windows in the WebDriver
	// session
	@AfterSuite
	public void teardown() {
		driver.close();
		driver.quit();
	}

	// Abstract method to enforce the sub classes to provide their
	// implementation to their respective tabs
	public abstract void gotoTab();
}
