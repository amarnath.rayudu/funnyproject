package com.worldclock.automation.contact;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

//import com.worldclock.automation.base.WorldclockBaseTest;

public class ContactNavigationTest extends ContactCommonTest {

	@Test
	public void contactLaunchTest() {
		gotoTab();

	}

	@Test
	public void contactVerificationTest() {
		assertTrue(driver.findElements(By.xpath("//h4[contains(text(),'W')]")).size() > 0, "Text not found");
	}

	@Test
	public void navVerificationTest() {
		driver.findElement(By.xpath("//i[@class='fa fa-chevron-right']")).click();
		driver.navigate().back();
	}

}
