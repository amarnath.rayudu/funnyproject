package com.worldclock.automation.home;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

//import com.worldclock.automation.base.WorldclockBaseTest;
// Test cases for header verification 
public class HeaderVerificationTest extends HomeCommonTest {

	@Test
	public void logoVerificationTest() {
		assertTrue(driver.findElements(By.xpath("//img[@src='http://worldclock.com/wp-content/themes/twentysixteen/img/logo-active.png']")).size()>0,"Logo is not available");
		
		}
	
	@Test
	public void socialVerificationTest() {
		assertTrue(driver.findElements(By.xpath("//li[@class='socials pull-right']")).size()>0,"Social tab is not available");
		
	}
	
	@Test
	public void tabVerificationTest() {
		assertTrue(driver.findElements(By.xpath("//a[contains(text(),'Home')]")).size()>0,"Home tab not found");
		assertTrue(driver.findElements(By.xpath("//a[contains(text(),'Time Zone Map')]")).size()>0,"time zone tab not found");
		assertTrue(driver.findElements(By.xpath("//a[contains(text(),'Conver')]")).size()>0,"time zone converter not found");
		
	}
}
