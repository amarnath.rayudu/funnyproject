package com.worldclock.automation.home;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
// Test cases for HomeContentTest
//import com.worldclock.automation.base.WorldclockBaseTest;

public class HomeContentTest extends HomeCommonTest {

	@Test
	public void flagVerificationTest() {
		assertTrue(driver.findElements(By.xpath("//img[@id='location-flag']")).size() > 0, "Flag not found");
	}

	@Test
	public void localTimeVerificationTest() {
		assertTrue(driver.findElements(By.xpath("//h4[contains(text(),'Major')]")).size() > 0,"Major cities not found");
	}

	@Test
	public void readMoreTest() {
		assertTrue(driver.findElements(By.xpath("//a[contains(text(),'Read more')]")).size() > 0,"Read more not found");
	}

}
