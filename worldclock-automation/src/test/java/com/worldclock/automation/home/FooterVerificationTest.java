package com.worldclock.automation.home;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

//import com.worldclock.automation.base.WorldclockBaseTest;

public class FooterVerificationTest extends HomeCommonTest {
	//**Test Case for Email verification**//
	@Test
	public void emailboxTest() {
		assertTrue(driver.findElements(By.xpath("//footer//input[@name='email']")).size()>0,"emailbox not found");
	}
	
	@Test
	public void copyrightTest() {
		assertTrue(driver.findElements(By.xpath("//p[contains(text(),'All')]")).size()>0,"copyright not found");
	}
	
	@Test
	public void contactUsTest() {
		assertTrue(driver.findElements(By.xpath("//a[contains(text(),'Contact Us � World Clock')]")).size()>0,"contact us not found");
	}

}
